**Quiz Questions:**

1. **Question 1:** What is the purpose of using procedures in Oracle?
a) To return a value
b) To perform calculations
c) To group SQL statements
d) To store data in tables
Answer:c

2. **Question 2:** Which type of parameter can be used in both input and output operations?
a) IN
b) OUT
c) IN OUT
d) NONE
Answer:c

3. **Question 3:** How are local variables declared in a procedure?
a) Using the DECLARE keyword
b) Using the LOCAL keyword
c) Using the VAR keyword
d) Local variables are not allowed in procedures
Answer:a

4. **Question 4:** Which keyword is used to call a procedure from an anonymous PL/SQL block?
a) BEGIN
b) EXECUTE
c) CALL
d) DECLARE
Answer:b

5. **Question 5:** What is the main difference between a function and a procedure in Oracle?
a) Functions can have input parameters, while procedures cannot.
b) Procedures can return a value, while functions cannot.
c) Functions are used for calculations, while procedures are used for data storage.
d) There is no difference between functions and procedures in Oracle.
Answer:b

6. **Question 6:** Which clause is used to specify the return type of a function in Oracle?
a) RETURNS
b) RESULT
c) RETURN
d) RESULTSET
Answer:c

7. **Question 7:** What happens when a function is called from a SQL query?
a) The function's name is replaced with its code.
b) Ththe RETURN; statement.
c) Use e function is executed, and its return value is used in the query.
c) The function is ignored in the query.
d) The query returns an error.
Answer:b

8. **Question 8:** In a function, how can you return a NULL value if no result is found?
a) Use the RETURN NULL; statement.
b) Use the RETURN 0; statement.
d) Use the RETURN 'NULL'; statement.
Answer:a

9. **Question 9:** Which of the following is NOT a valid datatype for a function parameter?
a) NUMBER
b) VARCHAR2
c) DATE
d) BOOLEAN
Answer:c

10. **Question 10:** What is the purpose of using functions in Oracle?
a) To perform actions or tasks
b) To group SQL statements
c) To return a single result
d) To store data in tables
Answer:c

**Open-Ended Questions:**

1. **Question 1:** Explain the concept of encapsulation in the context of procedures and functions in Oracle.
Prosedurlar və funksiyalar PL/SQL-in vacib komponentləridir, məntiqi əhatə etmək, təkrar istifadəni inkişaf etdirmək və kodun təşkilini təkmilləşdirmək üçün bir yol təqdim edir. Qaytarma dəyəri olmayan tapşırıqlar üçün prosedurlar, hesablamalar və qaytarılan dəyərlər üçün funksiyalar istifadə olunur.

2. **Question 2:** How do you pass values to a procedure as input parameters? Provide an example.
Prosedurun parametrlər siyahısında daxiletmə parametrlərini təyin etməklə və sonra proseduru çağırarkən dəyərləri təqdim etməklə dəyərləri prosedura giriş parametrləri kimi ötürə bilərik.
CREATE OR REPLACE PROCEDURE INSERT_EMPLOYEE(
    emp_id NUMBER,
    emp_name VARCHAR2
) AS
BEGIN
    INSERT INTO employees (employee_id, first_name) VALUES (emp_id, emp_name);
    COMMIT;
END;
/

3. **Question 3:** What is the difference between an input parameter and an output parameter in a procedure?
İnput parametrləri istifadəçi tərəfindən daxil olunan parametrlərdir,output parametrləri isə nəticədə alınan parametrlərdir.

4. **Question 4:** Write a PL/SQL function that calculates the factorial of a given number.
CREATE OR REPLACE FUNCTION CALCULATE_FACTORIAL(n NUMBER) 
RETURN NUMBER
AS
BEGIN
    IF n = 0 THEN
        RETURN 1;
    ELSE
        RETURN n * CALCULATE_FACTORIAL(n - 1);
    END IF;
END;
/
DECLARE
    num NUMBER := 4;
    factorial NUMBER;
BEGIN
    factorial := CALCULATE_FACTORIAL(num);
    DBMS_OUTPUT.PUT_LINE('Factorial of ' || num || ' is: ' || factorial);
END;
/

5. **Question 5:** Compare and contrast the usage of functions in SQL queries and PL/SQL blocks. Provide examples for each case.
SQL sorğularında funksiyalar verilənlərin hesablama və dəyişikliklərini həyata keçirmək üçün istifadə olunur. Funksiyalar SQL sorğusununhissələrində, məsələn, SELECT, WHERE, GROUP BY, ORDER BY və s. də istifadə oluna bilər.
Nümunə:
SELECT employee_id, LOWER(first_name) AS lowercase_name
FROM employees;
PL/SQL bloklarında funksiyalar əsasən müəyyən tapşırıqları yerinə yetirmək üçün istifadə olunur, çox vaxt tək bir nəticə qaytarırlar.
CREATE OR REPLACE FUNCTION CALCULATE_RECTANGLE_AREA(
    length NUMBER,
    width NUMBER
) RETURN NUMBER AS
    area NUMBER;
BEGIN
    area := length * width;
    RETURN area;
END;
/
DECLARE
    rectangle_length NUMBER := 5;
    rectangle_width NUMBER := 8;
    area_result NUMBER;
BEGIN
    area_result := CALCULATE_RECTANGLE_AREA(rectangle_length, rectangle_width);
    DBMS_OUTPUT.PUT_LINE('Area of rectangle: ' || area_result);
END;
/

